// ESP32 -> Wifi -> Wificlientbasic
#include <WiFi.h>
#include <WiFiMulti.h>
#include <HTTPClient.h>
#include <DHT.h>
#define DHTPIN 15
#define DHTTYPE DHT11
#include "config.h"
int d = 1000;
HTTPClient http;
HTTPClient http_send;
WiFiMulti WiFiMulti;

void led (int numled) {
  digitalWrite(numled, HIGH);
  delay(200);
}
DHT sensor(DHTPIN, DHTTYPE);
void setup()
{
  pinMode(13, OUTPUT);
  pinMode(14, OUTPUT);
  //Openning Serial Port @ 115200
  Serial.begin(115200);
  sensor.begin();//init sensor
  // Waiting 10 ms (WHY??????)
  // Connecting to The AP
  WiFiMulti.addAP("POP_SENSORS", "P0PS3NS0RS!");
  // While the wifi is note connected, we retry and wait 500 ms
  while (WiFiMulti.run() != WL_CONNECTED) {
    Serial.print(".");
  }
  //Display the Ip adress if it's connected.
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  // We Are Ready !!
}
void send_data() {
  delay(d);
  float h = sensor.readHumidity();
  float t = sensor.readTemperature();
  String host = "10.200.204.25:1880";
  //Url to Contact/Connect
  String url = "/sensor/g09?temp=" + String (t) + "&hygro=" + String (h);
  http_send.begin("http://" + host + url);
  int retCode = http_send.GET();
  Serial.print(" [HTTP] ""Return Code = ");
  Serial.println(retCode);
  if (retCode == HTTP_CODE_OK) {
    String content = http_send.getString();
    Serial.println(content);
    led(13);
  }
  else {
    // An Error occured ....
    Serial.print ("[HTTP] GET failed, error : ");
    Serial.println(retCode);
    led(14);
  }
  Serial.print("Humidité :");
  Serial.println(h);
  Serial.print("Température : ");
  Serial.println(t);
  http_send.end();
}


void get_data() {
  delay(d);
  float h = sensor.readHumidity();
  float t = sensor.readTemperature(); //Url to Contact/Connect String host = "api.openweathermap.org";
  //url to get:
    String host = "http://api.openweathermap.org";

  String url = "/data/2.5/forecast?q=willems,fr&appid=189f4126f90265409b820215c69178b5";
  http.begin("http://" + host + url);
  int retCode = http.GET();
  Serial.print("[HTTP] ""Return Code =");
  Serial.println(retCode);
  if (retCode == HTTP_CODE_OK) {
    String content = http.getString();
    Serial.println(content);
    led(13);
    http.end();
  }
}
void loop()
{ send_data();
  get_data();
  delay(5000);
}
